import os


def notes_to_comments(infile, outfile):
    with open(infile, 'r', encoding='utf-8') as file:
        lines = file.read()
        lines = lines.replace("Note Note", "Note Comment")
        file.close()
        with open(outfile, 'w+', encoding='utf-8') as out:
            out.write(lines)
            out.close()


def lyx_to_latex(lyxfile):
    os.system("lyx -e pdflatex " + lyxfile)
