from pathlib import Path
import re

def get_exercise_num_as_string(filename):
    name = Path(filename).name
    ueb = re.search(r'\d+', name).group(0)
    return ueb


def create_ex_name(filename):
    file = Path(filename).name
    #file: "ueb1-ibn-2019.lyx"
    #name = "Ü-" + file[-6] + file[-5] + "-" + get_exercise_num_as_string(file) + "-"
    name = "Ü-" + (abs(int(re.findall(r'\d+', file)[-1]))%100) + "-" + get_exercise_num_as_string(file) + "-"
    return name


def separate_exercises(latex_file, out_folder):
    # takes a latex file and separates the individual exercises into folder out_path
    with open(latex_file, encoding='utf-8') as mainFile:
        stop_string = "aufgabe{"
        work_array = []
        ex_count = 0
        for line in mainFile:
            if stop_string in line:
                other_filename = create_ex_name(latex_file) + str(ex_count)
                ex_count += 1
                # lyx export does not always put \aufgabe in new line
                exercise = line.split('\\aufgabe')
                exercise[1] = '\\aufgabe' + exercise[1]
                work_array.append(exercise[0])
                with open(out_folder.absolute() / (other_filename + ".tex"), "w+", encoding='utf-8') as file:
                    file.writelines(work_array)
                    file.close()
                work_array = [exercise[1]]
                continue
            if "\\end{document}" not in line:
                work_array.append(line)
        other_filename = create_ex_name(latex_file) + str(ex_count)
        with open(out_folder.absolute() / (other_filename + ".tex"), "w+", encoding='utf-8') as file:
            file.writelines(work_array)
            file.close()
        mainFile.close()
