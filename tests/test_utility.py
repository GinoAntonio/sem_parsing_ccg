import pytest

from semantic_pandas import decategorize, deoptionalize, sequencesAreSimilar


class TestUtility:
    @pytest.mark.parametrize("token", [
        "$", "$Category", "$category", "$c", "$?", "$?category"
    ])
    def testDecategorize(self, token):
        expectedToken = token[1:]

        assert decategorize(token) == expectedToken

    @pytest.mark.parametrize("token", [
        "", "category", "c"
    ])
    def testDecategorizeInvalid(self, token):
        with pytest.raises(ValueError):
            decategorize(token)

    @pytest.mark.parametrize("token", [
        "?", "?Category", "?category", "?c", "?$c"
    ])
    def testDeoptionalize(self, token):
        expectedToken = token[1:]

        assert deoptionalize(token) == expectedToken

    @pytest.mark.parametrize("token", [
        "", "category", "c"
    ])
    def testDeoptionalizeInvalid(self, token):
        with pytest.raises(ValueError):
            deoptionalize(token)

    @pytest.mark.parametrize("tokens1, tokens2, threshold, areSimilar", [
        ("dog", "dog", 1, True),
        ("dog", "cat", 1, False),
        ("dog", "cat", 0, True),
    ])
    def testSequencesAreSimilar(self, tokens1, tokens2, threshold, areSimilar):
        assert sequencesAreSimilar(tokens1, tokens2, threshold) == areSimilar
