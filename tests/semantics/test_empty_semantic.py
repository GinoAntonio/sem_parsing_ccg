from semantic_pandas.semantics import EmptySemantic


class TestEmptySemantic:
    def testCompute(self):
        semantic = EmptySemantic()

        assert semantic.compute() == dict()
