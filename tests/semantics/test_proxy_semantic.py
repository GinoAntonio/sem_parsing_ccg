import pytest

from semantic_pandas.semantics import ProxySemantic


class TestProxySemantic:
    @pytest.mark.parametrize("proxyPosition, semanticValues", [
        (0, [{"value": "key"}]),
        (1, [{}, {"value": "key"}])
    ])
    def testCompute(self, proxyPosition, semanticValues):
        semantic = ProxySemantic(proxyPosition)

        assert semantic.compute(semanticValues) == semanticValues[proxyPosition]

    @pytest.mark.parametrize("proxyPosition, semanticValues", [
        (0, []),
        (1, [{"value": "key"}])
    ])
    def testComputeInvalid(self, proxyPosition, semanticValues):
        semantic = ProxySemantic(proxyPosition)

        with pytest.raises(IndexError):
            assert semantic.compute(semanticValues)
