from setuptools import find_packages, setup

requirements = [
    "scikit-learn",
    "scipy",
    "spacy",
    "en_core_web_md @ https://github.com/explosion/spacy-models/releases/download/"
    "en_core_web_md-2.3.1/en_core_web_md-2.3.1.tar.gz"
]

language_server_requirements = [
    "pygls >=0.9, <1",
    "editdistance"
]

dev_requirements = [
    "pylint >=2.5, <3",  # NOTE: pylint 2.5 introduces breaking changes
    "flake8",
    "pytest",
    "sphinx",
    "sphinx_rtd_theme",
    "coverage"
]

setup(
    name="semantic_pandas",
    version="0.0.1",
    description="Semantic parsing bachelor thesis",
    author="Robin Mader",
    python_requires=">=3.8",
    install_requires=requirements + language_server_requirements,
    extras_require={"dev": dev_requirements},
    author_email="r.mader@stud.uni-heidelberg.de",
    packages=find_packages(
        include=["semantic_pandas", "semantic_pandas.*"],
        exclude=["*.tests", "*.tests.*", "tests.*", "tests", "examples", "examples.*"]
    )
)
