# Semantic Pandas
`semantic_pandas` is a python package implementing semantic parsing based on combinatory
categorial grammars. Specialized classes, functionalities and models have been developed for
parsing utterances specific to the python `pandas` domain. This package has been developed during
the course of a bachelor thesis of which a copy can be found in the
[misc/thesis](misc/thesis) directory. Please see the thesis for more detailed information
about the implementation and experiment results.

Additionally, the package features a language server which allows for integration with
corresponding language clients. An example of such a client for the Visual Studio Code IDE has
been developed and can be found in the [language_client](language_client) directory.

The implemented algorithms have been heavily inspired by those implemented in the
[SippyCup project](https://github.com/wcmac/sippycup). The code developed in this repository is
intended for educational and research purposes. Use this code in a production environment at your
own risk.


# Installation
You can use the [interactive installation Powershell script](scripts/install.ps1) to install
the various parts of this package, alternatively you can follow the instructions described
below. There usually is a distinction in installing them for simple usage or development purposes.


## Python package
The python package can be installed by executing one of the commands below.

If you wish to install the package in a virtual environment, remember to activate it before
execution.

Depending on your OS, it may be necessary to install system specific dependencies (e.g. `apt-get install build-essential`).

```sh
# use this for installing the package:
# NOTE: this will not install the machine learning models which must be
#   either copied to the corresponding directory or baked using the
#   bake_data script. To automatically use the default models distributed
#   in this repository use the '-e' flag for installation.
python -m pip install .

# use this for installing the package in development mode:
python -m pip install -e .[dev]
```


## Visual Studio Code extension
The Visual Studio Code extension can be installed as described below. Installing requires
`npm` to be installed on the system. Note that the extension requires the VS Code
`python.pythonPath` configuration to reference a valid python executable which has the
`semantic_pandas` package installed. If you simply wish to demo the VS Code extension, it is
recommended to install the Python package and VS Code extension in development mode.

1. Execute the following in the root directory of this repository.
    ```sh
    cd language_client
    npm i
    ```
2. If you wish to install the extension for a Visual Studio Code instance use
    ```sh
    cd language_client
    mkdir dist
    npm run package
    ```
    This will create a `.vsix` file in the [language_client/dist](language_client/dist) directory
    which can then be installed using the VS Code CLI or GUI (see the corresponding documentation
    for more information).

    For development purposes, the extension can also be started using the Visual Studio Code
    debugger: Open the repository in VS Code. Navigate to the "Run" tab on the sidebar. Choose
    the corresponding launch configuration from the dropdown at the top. Choose the
    `Server + Client (Debug)` configuration to start the extension in debug mode, which
    separately starts the server and client and sets communication mode to TCP. Choose the
    `Launch Client (Prod)` configuration to start the extension in production mode, which
    starts the client which automatically starts the server and sets communication mode to IO.
    This will start a new VS Code instance which has the extension installed.

    **WARNING**: If you are using a virtual environment or installed the `semantic_pandas`
    package in a Python environment that is not your VS Code default, you may have to configure
    the `python.pythonPath` configuration variable to point to the correct Python executable
    and restart VS Code in order for the extension to work correctly.

3. Once you have installed the extension, simply open a new Python file (make sure that the
    language mode of the file is set to "Python" in the lower right) and type a new line
    starting with `##` (leading whitespaces are ignored) followed by the utterance to parse.
    To parse the utterance, trigger the auto completion feature (default shortcut: `CTRL + SPACE`)
    while your cursor is at the end of the line. This will offer a list of corresponding, parsed
    snippets. Alternatively, you can use the code action feature (default shortcut: `F1`) while
    your cursor is on the corresponding line and select the `Semantic Pandas: Parse` action.
    This will insert the generated snippet below the current line.

## Installation on gitpod
For a quick installation on gitpod the repository can be opened in a gitpod workspace. This will
automatically install the Python package and create a .vsix file. Since gitpod does not feature
automatic installation of .vsix files, the following steps must be done to install and preview
the extension:
1. Download the .vsix file from the language_client/dist directory.
2. Open the extension tab on gitpod from the sidebar.
3. Drag the downloaded .vsix file into the extension tab.
4. The extension can now be used in Python files.

# Models
The parse scoring implementation in this project is configured to be able to use different
combinations of `scikit-learn` scaling, reducing and scoring models. For space reasons, only one
combination of models is distributed in this repository. If you wish to use different models,
you must either train them yourself or use the `semantic_pandas.scripts.bake_data` module to
create all module combinations of a given target feature count. Note that running this script
can take a very long time and consume a large amount of computing resources (>3 hours, >8GB RAM).
The labels of the available model types are:
- **scaler labels**: `none`, `standard`, `maxabs` (`none` is not an actual scaler, but rather the absence of
    a scaler)
- **reducer labels**: `sparserandom`, `featureagg`, `univariate`, `pca`
- **scoring model labels**: `perceptron`, `kneighbors`, `tree`, `svr`, `gradientboosting`, `forest`

Created models can be loaded using the `loadResource` function. Naming conventions for the resource
identifiers are:
- **scaler**: "{scaler label}\_scaler\_{size suffix}"
- **reducer**: "{scaler label}\_{reducer label}\_reducer\_{size suffix}"
- **scoring model**: "{scaler label}\_{reducer label}\_{model label}\_model\_{size suffix}"

Valid size suffixes are sm, md, lg and xl.


# Development
If you wish to contribute to this project, feel free to fork it. Since development on this project
may cease after completion of the corresponding bachelor thesis, it is possible that pull
requests may not be accepted for a long time. Remember to view the [LICENSE](LICENSE) file
before redistributing this code.


# License
This project is distributed under the MIT license of which a copy can be found in the
[LICENSE](LICENSE) file.
