"""
Class module for the `AnnotatorRule` class.
Please see `help(AnnotatorRule)` for more information.
"""

from typing import Sequence

from ..annotators.annotator import Annotator
from ..semantics import CustomSemantic

from .rule import Rule


class AnnotatorRule(Rule):
    """Rule type for encapsulating an annotator."""

    def __init__(self, lhs: str, annotator: Annotator):
        """Please see `help(AnnotatorRule)` for more information."""
        super().__init__(
            lhs,
            tuple(),
            CustomSemantic(annotator.annotate)
        )

    def isValid(self, tokens: Sequence[str]) -> bool:
        """Determine wether the annotator rule can be applied to the given tokens."""
        return self.semantic.compute(tokens) is not None
