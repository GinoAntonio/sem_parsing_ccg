"""
Class module for the `LexicalRule` class.
Please see `help(LexicalRule)` for more information.
"""

from typing import Union, Iterable

from ..semantics.semantic import Semantic
from ..utility import isCategory

from .rule import Rule


class LexicalRule(Rule):
    """Rule representing a rule using a category as rhs and a token as lhs."""
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def __init__(
            self,
            lhs: str,
            rhs: Union[str, Iterable[str]],
            semantic: Semantic = None):
        """Please see `help(LexicalRule)` for more information."""

        if isinstance(rhs, str):
            rhs = rhs.split()
        rhs = tuple(rhs)

        if any([isCategory(token) for token in rhs]):
            raise Exception("Lexical rule contains category.")

        super().__init__(lhs, rhs, semantic)
