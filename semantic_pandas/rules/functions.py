"""Module containing utility functions for rules."""

from typing import Iterable, Union

# cyclic import is necessary and handled by importing the module instead of the class
from . import n_ary_rule  # pylint: disable=cyclic-import
from .rule import Rule
from .lexical_rule import LexicalRule
from .unary_rule import UnaryRule
from .binary_rule import BinaryRule
from ..semantics.semantic import Semantic
from ..semantics.empty_semantic import EmptySemantic
from ..utility import tokenize, isCategory


def makeRule(lhs: str, rhs: Union[str, Iterable[str]], semantic: Semantic = None) -> Rule:
    """Create a rule by inferring its type from the given arguments."""
    if isinstance(rhs, str):
        rhs = tuple(tokenize(rhs))

    if semantic is None:
        semantic = EmptySemantic()

    rule = None
    if all([not isCategory(token) for token in rhs]):  # lexical rule
        rule = LexicalRule(lhs, rhs, semantic=semantic)
    else:
        if any([not isCategory(token) for token in rhs]):
            raise ValueError("Rules mixes terminal and non-terminal tokens.")

        if len(rhs) == 1:  # unary rule
            rule = UnaryRule(lhs, rhs, semantic=semantic)
        elif len(rhs) == 2:  # binary rule
            rule = BinaryRule(lhs, rhs, semantic=semantic)
        else:  # n-ary rule
            rule = n_ary_rule.NAryRule(lhs, rhs, semantic=semantic)

    return rule
