"""Utility module for the PandasQuery data class."""

from collections import namedtuple

PandasQuery = namedtuple("PandasQuery", ["intent", "snippet"])
