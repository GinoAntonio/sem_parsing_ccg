"""
Class module for the `SimpleNumberAnnotator` class.
Please see `help(SimpleNumberAnnotator)` for more information.
"""

from typing import Sequence, Dict, Any

from .annotator import Annotator


class SimpleNumberAnnotator(Annotator):
    """
    A simple annotator for annotating numbers.

    Supported numbers are:
    - All numbers as plain digits
    - 0 - 10 (non negative) as words ('zero', 'one', 'two', etc.)
    """
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    tokenToValue = {
        "zero": 0,
        "one": 1,
        "two": 2,
        "three": 3,
        "four": 4,
        "five": 5,
        "six": 6,
        "seven": 7,
        "eight": 8,
        "nine": 9,
        "ten": 10
    }

    def annotate(self, tokens: Sequence[str]) -> Dict[str, Any]:
        """
        Return a dictionary representing the semantics of the annotated tokens.

        The returned semantics are a dictionary of the shape:
        {'value': int}
        """
        value = None
        if len(tokens) == 1:
            token = tokens[0]

            if token in self.tokenToValue:
                value = self.tokenToValue[token.lower()]
            else:
                try:
                    value = float(tokens[0])
                    if int(value) == value:
                        value = int(value)
                except ValueError:
                    pass

        annotation = None if value is None else {"value": value}

        return annotation
