"""Configuration constants for the language server."""

import re
import logging

MAX_AUTO_COMPLETION_ITEMS = 3
SEMANTIC_PARSE_PREFIX = "##"  # NOTE: should not start with a space
SEMANTIC_PARSE_TRIGGER_RE = r"^\s*" + re.escape(SEMANTIC_PARSE_PREFIX) + r".*$"
SEMANTIC_PARSE_LINE_COMMAND = "semanticParseLine"
TAB_STOP_PLACEHOLDER_RE = r"{(\w*?)}"

LOGGER_NAME = "semanticPandas"
LOGGER = logging.getLogger(LOGGER_NAME)
