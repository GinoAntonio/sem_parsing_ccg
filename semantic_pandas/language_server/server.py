"""Language server to be used with a corresponding language server client."""

import re
from typing import Any, List

from pygls.server import LanguageServer
from pygls.features import CODE_ACTION, COMPLETION
from pygls.types import (CodeActionParams, Command, CompletionList,
                         CompletionParams, Position, Range, TextEdit,
                         WorkspaceEdit, CompletionItem, CompletionItemKind,
                         InsertTextFormat, MessageType)

from ..evaluators import PandasSemanticEvaluator
from ..grammars import pandasGrammar
from .utility import extractIntentFromLine, insertTabStops
from ..utility import loadResource
from ..semantic_parser import SemanticParser
from ..sklearn_parse_scorer import SklearnParseScorer

from .constants import (SEMANTIC_PARSE_TRIGGER_RE, SEMANTIC_PARSE_LINE_COMMAND, LOGGER,
                        MAX_AUTO_COMPLETION_ITEMS)

# create language server
server = LanguageServer()

# create semantic parser
# scaler = None
reducer = loadResource("none_univariate_reducer_lg")
scoringModel = loadResource("none_univariate_perceptron_model_lg")

if reducer is None:
    LOGGER.warning("Reducer is None. Check if the resource name has been set correctly.")

if scoringModel is None:
    LOGGER.warning("Scoring model is None. Check if the resource name has been set correctly.")

scorer = SklearnParseScorer(
            scoringModel,
            lambda parse: parse.featureVector(pandasGrammar),
            [reducer])

parser = SemanticParser(pandasGrammar, scorer)
evaluator = PandasSemanticEvaluator()


@server.feature(COMPLETION)
def completions(languageServer: LanguageServer, params: CompletionParams) -> CompletionList:
    """Return a list of auto completion items to VS Code for the current expression."""
    LOGGER.debug("Completion request received.")

    doc = languageServer.workspace.get_document(params.textDocument.uri)
    lineNumber = params.position.line

    # ensure that current line is within document
    # (out of bounds can occur while appending new lines to a document)
    if lineNumber >= len(doc.lines):
        LOGGER.debug("Current line outside of document bounds.")
        return CompletionList(False, [])

    line = doc.lines[lineNumber]

    # remove potential linebreaks from end of line
    if line.endswith("\n"):
        line = line[:-1]

    # only auto complete if the line matches the trigger regex
    if re.fullmatch(SEMANTIC_PARSE_TRIGGER_RE, line) is None:
        LOGGER.debug(
            f"Current line '{line}' does not match trigger pattern '{SEMANTIC_PARSE_TRIGGER_RE}'."
        )
        return CompletionList(False, [])

    # extract intent
    intent = extractIntentFromLine(line)

    # get parses
    parses = parser.parseAll(intent)

    # generate snippets
    snippets = []
    for parse in parses:
        snippet = evaluator.evaluate(parse)

        # do not add "None" or duplicate snippets
        if snippet is None or snippet in snippets:
            continue

        snippets.append(snippet)

        # stop if we have enough snippets
        if len(snippets) >= MAX_AUTO_COMPLETION_ITEMS:
            break

    # create completion items
    completionItems = []
    for snippet in snippets:
        item = CompletionItem(
            snippet,  # label
            CompletionItemKind.Snippet,  # completion item kind
            # use last typed word as filter text to prevent automatic filtering of snippets
            filter_text=line.split(" ")[-1],
            # the text to insert in place of the typed text
            insert_text=line.split(" ")[-1] + "\n" + insertTabStops(snippet),
            insert_text_format=InsertTextFormat.Snippet
        )
        completionItems.append(item)

    LOGGER.debug(
        f"Generated {len(snippets)} item{'s' if len(snippets) != 1 else ''} for auto completion"
    )

    return CompletionList(False, completionItems)


@server.feature(CODE_ACTION)
def codeActions(languageServer: LanguageServer, params: CodeActionParams):
    """Create a code action for the current line in VS Code."""
    LOGGER.debug("Code action request received.")

    # only enable code action if a single line is selected
    if params.range.start.line != params.range.end.line:
        LOGGER.debug("Multiple lines selected.")
        return []

    doc = languageServer.workspace.get_document(params.textDocument.uri)
    line = doc.lines[params.range.start.line]

    # remove potential linebreaks from end of line
    if line.endswith("\n"):
        line = line[:-1]

    # only offer code action if line starts with appropriate prefix
    if re.fullmatch(SEMANTIC_PARSE_TRIGGER_RE, line) is None:
        LOGGER.debug(
            f"Current line '{line}' does not match trigger pattern '{SEMANTIC_PARSE_TRIGGER_RE}'."
        )
        return []

    # create reference to command which does the actual text parsing & editing
    commandArgs = [{
        "documentUri": params.textDocument.uri,
        "line": params.range.start.line
    }]
    command = Command(
        "Semantic Pandas: Parse",
        SEMANTIC_PARSE_LINE_COMMAND,
        commandArgs
    )

    return [command]


@server.command(SEMANTIC_PARSE_LINE_COMMAND)
def semanticParseLine(languageServer: LanguageServer, args: List[Any]):
    """Parse the current line in VS Code and insert the most likely snippet below it."""
    LOGGER.debug("Semantic parse line command called.")

    # retrieve information about the line to parse
    if len(args) > 0:
        args = args[0]  # first entry is a dictionary with named arguments
        docUri = args.documentUri
        doc = languageServer.workspace.get_document(docUri)
        lineNumber = int(args.line)
        line = doc.lines[lineNumber]
        lineEnd = len(line)
    else:
        languageServer.show_message(
            "Please use the code action feature to execute this command instead of manual"
            "execution."
        )

    # extract intent from line
    intent = extractIntentFromLine(line)

    # parse line
    parse = parser.parse(intent)
    snippet = evaluator.evaluate(parse)

    if snippet is not None:
        snippet = insertTabStops(snippet)  # DEBUG
        # create document edit

        # dictionary mapping document URIs to a list of edits to apply
        changes = {
            docUri: [
                TextEdit(
                    Range(
                        Position(lineNumber, lineEnd),
                        Position(lineNumber, lineEnd)
                    ),
                    "\n" + snippet)
            ]
        }
        edit = WorkspaceEdit(changes=changes)

        # request client to apply the edit
        languageServer.apply_edit(edit)
    else:
        languageServer.show_message("Utterance did parse into a valid snippet.", MessageType.Error)
