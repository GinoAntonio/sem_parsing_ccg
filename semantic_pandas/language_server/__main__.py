"""Main file for the language server with CLI support."""

import logging
import sys
import argparse

from .constants import LOGGER
from .server import server


def configureLogger(logger: logging.Logger):
    """Configure the logger."""
    # NOTE: change logging level here to change it for the entire extension
    logger.level = logging.ERROR


def parseArguments():
    """Parse arguments passed to the process."""
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--tcp", action="store_true",
        help="Use TCP instead of stdio"
    )
    parser.add_argument(
        "--host", default="127.0.0.1",
        help="IP address to which to bind the server. Ignored if not in TCP mode."
    )
    parser.add_argument(
        "--port", type=int, default=8080,
        help="Port on which to listen. Ignored if not in TCP mode."
    )

    return parser.parse_args()


def main():
    """Run the language server, ready to be connected to from clients."""
    configureLogger(LOGGER)

    args = parseArguments()

    LOGGER.info("Starting language server.")

    if args.tcp:
        LOGGER.info("Starting in TCP mode.")
        server.start_tcp(args.host, args.port)
    else:
        LOGGER.info("Starting in IO mode.")
        server.start_io()


if __name__ == "__main__":
    sys.exit(main())
