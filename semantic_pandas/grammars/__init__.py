"""Module for pre-defined grammars for semantic parsing."""

from .pandas_grammar import pandasGrammar

__all__ = ["pandasGrammar"]
