"""Module containing handcrafted TB1 queries dataset for parser testing/training."""

import random
from collections import namedtuple

Tb1Query = namedtuple("PandasQuery", ["intent", "snippet"])

random.seed(3042330287)  # use seed for reproducable results

manualTB1Queries = [
    #split
    Tb1Query("split this string", "{current string}.split()" ),
    Tb1Query("split this string at space", "{current string}.split()" ),
    Tb1Query("split this string at comma", "{current string}.split(',')" ),
    Tb1Query("split this string at semicolon", "{current string}.split(';')" ),
    Tb1Query("split this string at point", "{current string}.split('.')" ),

    #replace
    Tb1Query("change all comma to points", "{current string}.replace(',', '.')" ),
    Tb1Query("change all comma to points in the text", "{current file}.replace(',', '.')" ),
    Tb1Query("replace all comma with points", "{current string}.replace(',', '.')" ),
    Tb1Query("make all commas to points", "{current string}.replace(',', '.')" ),
    Tb1Query("change all comma to points", "{current string}.replace(',', '.')" ),

    #remove
    Tb1Query("remove all blanks", "{current string}.strip()"),
    Tb1Query("remove whitespaces", "{current string}.strip()"),
    Tb1Query("eliminate blanks", "{current string}.strip()"),
    Tb1Query("get rid of blanks", "{current string}.strip()"),
    Tb1Query("remove quotation marks", "{current string}.strip('\"')"),

    #regex
    Tb1Query("display all digits in text", "re.findall('[0-9]+', {current file}})"),
    Tb1Query("show all numbers", "re.findall('[0-9]+', {current file}})" ),
    Tb1Query("find all figures in the text", "re.findall('[0-9]+', {current file}})" ),
    Tb1Query("find postcodes", "re.findall('\d{5}', string)[0]"),
    Tb1Query("show all figures that can match a postcode, ", "re.findall('\d{5}', string)[0]"),

]

random.shuffle(manualTB1Queries)

trainingmanualTB1Queries = \
    manualmanualTB1Queries[int(len(manualTB1Queries) * 0.1):]

testingmanualTB1Queries = \
    manualTB1Queries[:int(len(manualTB1Queries) * 0.1)]


