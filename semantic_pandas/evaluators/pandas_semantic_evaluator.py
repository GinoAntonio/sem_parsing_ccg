"""
Class module for the `PandasSemanticEvaluator` class.
Please see `help(PandasSemanticEvaluator)` for more information.
"""

from ..parse import Parse


class PandasSemanticEvaluator:
    """Evaluator for generating python pandas snippets from semantics."""
    # NOTE: class could theoretically be a singleton, this doesn't create a significant advantage
    #   though and is only slightly cleaner
    # pylint: disable=no-self-use
    # NOTE: few public methods are fine, due to OOP style used in this project
    # pylint: disable=too-few-public-methods

    def evaluate(self, parse: Parse) -> str:
        """Generate a python pandas snippet from the semantics of the given parse."""
        snippet = None
        semantics = parse.semantics
        if "domain" in semantics and semantics["domain"] == "pandas":
            if "operation" in semantics:
                operation = semantics["operation"]
                # create operation
                if operation == "create":
                    snippet = self._makeCreateSnippet(semantics)

                # select operation
                elif operation == "select":
                    snippet = self._makeSelectSnippet(semantics)

                # convert operation
                elif operation == "convert":
                    snippet = self._makeConvertSnippet(semantics)

                # append operation
                elif operation == "append":
                    snippet = self._makeAppendSnippet(semantics)

                elif operation == "sort":
                    snippet = self._makeSortSnippet(semantics)

        return snippet

    def _makeCreateSnippet(self, semantics: dict) -> str:
        typeArgument = semantics.get("type", None)
        fromArgument = semantics.get("from", {}).get("variable", "")
        assignTo = semantics.get("assignTo", {}).get("variable", None)
        if assignTo is None:
            assignTo = semantics.get("typeVariable", {}).get("variable", None)

        if typeArgument is None:
            typeArgument = "{class}"

        if fromArgument is None:
            fromArgument = ""

        if "index" in semantics:
            if semantics["index"] is None:
                index = "{index}"
            else:
                index = semantics["index"]["token"]

            fromArgument += f", index={index}"

        snippet = f"{typeArgument}({fromArgument})"

        if assignTo is not None and snippet is not None:
            snippet = f"{assignTo} = {snippet}"

        return snippet

    def _makeSelectSnippet(self, semantics: dict) -> str:
        selectType = semantics.get("selectType", None)
        assignTo = semantics.get("assignTo", {}).get("variable", None)

        snippet = None
        if selectType == "column":
            snippet = self._makeSelectColumnSnippet(semantics)
        elif selectType == "row":
            snippet = self._makeSelectRowSnippet(semantics)
        elif selectType == "maximum":
            snippet = self._makeSelectMaximumSnippet(semantics)
        elif selectType == "minimum":
            snippet = self._makeSelectMinimumSnippet(semantics)
        elif selectType == "average":
            snippet = self._makeSelectAverageSnippet(semantics)

        if assignTo is not None and snippet is not None:
            snippet = f"{assignTo} = {snippet}"

        return snippet

    def _makeSelectColumnSnippet(self, semantics: dict) -> str:
        # sourceType = semantics.get("selectSource", {}).get("type", None)
        sourceVariable = (
            semantics.get("selectSource", {})
                     .get("typeVariable", {})
                     .get("variable", "{dataframe}")
        )
        columnName = semantics.get("columnName", None)

        if columnName is not None:
            columnName = columnName.get("variable", None)
        if columnName is None:
            columnName = "{column}"

        if isinstance(columnName, list):
            # select multiple columns
            columnName = "[" + ", ".join(columnName) + "]"

        snippet = f"{sourceVariable}[{columnName}]"
        return snippet

    def _makeSelectRowSnippet(self, semantics: dict) -> str:
        # NOTE: could be refactored - split into smaller sub functions
        # pylint: disable=too-many-locals
        sourceVariable = (
            semantics.get("selectSource", {})
                     .get("typeVariable", {})
                     .get("variable", "{dataframe}")
        )
        conditionSemantics = semantics.get("condition", None)
        rangeSemantics = semantics.get("range", None)
        headSemantics = semantics.get("head", None)
        tailSemantics = semantics.get("tail", None)

        # handle condition semantics
        condition = None
        if conditionSemantics is not None:
            lhs = conditionSemantics.get("lhs", {}).get("variable", "{lhs}")
            rhs = conditionSemantics.get("rhs", {}).get("variable", "{rhs}")
            operator = conditionSemantics.get("operator", "{operator}")

            condition = f"{lhs} {operator} {rhs}"

        # handle range semantics
        rangeArgument = None
        if rangeSemantics is not None:
            rangeFromSemantics = rangeSemantics.get("from", None)
            rangeFrom = None
            if rangeFromSemantics is not None:
                rangeFrom = rangeFromSemantics.get("value", None)

            rangeToSemantics = rangeSemantics.get("to", None)
            rangeTo = None
            if rangeToSemantics is not None:
                rangeTo = rangeToSemantics.get("value", None)

            # ensure that rangeFrom is smaller than rangeTo, swap them if necessary
            if rangeFrom is not None and rangeTo is not None and rangeFrom > rangeTo:
                rangeFrom, rangeTo = rangeTo, rangeFrom

            rangeArgument = (rangeFrom, rangeTo)

        head = None
        if headSemantics is not None:
            head = headSemantics.get("value", None)

        tail = None
        if tailSemantics is not None:
            tail = tailSemantics.get("value", None)

        # build snippet
        snippet = f"{sourceVariable}"

        if rangeArgument is not None:
            rangeFromSnippet = rangeArgument[0] if rangeArgument[0] is not None else ""
            rangeToSnippet = rangeArgument[1] if rangeArgument[1] is not None else ""
            snippet += f"[{rangeFromSnippet}:{rangeToSnippet}]"

        if condition is not None:
            snippet += f"[{condition}]"

        if head is not None:
            snippet += f".head({head})"
        elif tail is not None:  # do not apply tail if a head condition exists
            snippet += f".tail({tail})"

        if condition is None and rangeArgument is None and head is None and tail is None:
            snippet += "[{condition}]"

        return snippet

    def _makeSelectMaximumSnippet(self, semantics: dict) -> str:
        selectSource = (semantics.get("selectSource", {})
                                 .get("typeVariable", {})
                                 .get("variable", "{dataframe}"))
        subSelectSource = (semantics.get("subSelectSource", {})
                                    .get("typeVariable", {})
                                    .get("variable", None))

        if subSelectSource is None and selectSource is not None:
            snippet = f"{selectSource}.max()"
        else:
            if subSelectSource is None:
                subSelectSource = "{column}"
            snippet = f"{selectSource}[{subSelectSource}].max()"

        return snippet

    def _makeSelectMinimumSnippet(self, semantics: dict) -> str:
        selectSource = (semantics.get("selectSource", {})
                                 .get("typeVariable", {})
                                 .get("variable", "{dataframe}"))
        subSelectSource = (semantics.get("subSelectSource", {})
                                    .get("typeVariable", {})
                                    .get("variable", None))

        if subSelectSource is None and selectSource is not None:
            snippet = f"{selectSource}.min()"
        else:
            if subSelectSource is None:
                subSelectSource = "{column}"
            snippet = f"{selectSource}[{subSelectSource}].min()"

        return snippet

    def _makeSelectAverageSnippet(self, semantics: dict) -> str:
        selectSource = (semantics.get("selectSource", {})
                                 .get("typeVariable", {})
                                 .get("variable", "{dataframe}"))
        subSelectSource = (semantics.get("subSelectSource", {})
                                    .get("typeVariable", {})
                                    .get("variable", None))

        if subSelectSource is None and selectSource is not None:
            snippet = f"{selectSource}.mean()"
        else:
            if subSelectSource is None:
                subSelectSource = "{column}"
            snippet = f"{selectSource}[{subSelectSource}].mean()"

        return snippet

    def _makeConvertSnippet(self, semantics: dict) -> str:
        convertFormat = semantics.get("convertFormat", {}).get("format", None)
        sourceVariable = semantics.get("typeVariable", {}).get("variable", None)
        assignTo = semantics.get("assignTo", {}).get("variable", None)

        if sourceVariable is None:
            sourceVariable = "{dataframe}"

        snippet = None
        if convertFormat == "csv":
            snippet = f"{sourceVariable}.to_csv()"
        elif convertFormat == "dict":
            snippet = f"{sourceVariable}.to_dict()"
        elif convertFormat == "json":
            snippet = f"{sourceVariable}.to_json()"
        elif convertFormat == "numpy":
            snippet = f"{sourceVariable}.to_numpy()"

        if assignTo is not None and snippet is not None:
            snippet = f"{assignTo} = {snippet}"

        return snippet

    def _makeAppendSnippet(self, semantics: dict) -> str:
        appendType = semantics.get("appendType", "row")
        appendSourceTarget = semantics.get("appendSourceTarget", {})
        appendSource = appendSourceTarget.get("source", {}).get("variable", "{dataframe1}")
        appendTarget = appendSourceTarget.get("target", {}).get("variable", "{dataframe2}")
        assignTo = semantics.get("assignTo", {}).get("variable", None)

        snippet = None
        if appendType == "row":
            snippet = f"concat([{appendSource}, {appendTarget}])"
        elif appendType == "column":
            snippet = f"concat([{appendSource}, {appendTarget}], axis=1)"

        if assignTo is not None and snippet is not None:
            snippet = f"{assignTo} = {snippet}"

        return snippet

    def _makeSortSnippet(self, semantics: dict) -> str:
        sourceVariable = semantics.get("typeVariable", {}).get("variable", None)
        orderBy = semantics.get("orderBy", {}).get("variable", None)
        assignTo = semantics.get("assignTo", {}).get("variable", None)

        if sourceVariable is None:
            sourceVariable = "{dataframe}"

        if orderBy is None:
            orderBy = ""
        elif isinstance(orderBy, list):
            orderBy = "[" + ", ".join(orderBy) + "]"

        snippet = f"{sourceVariable}.sort_values({orderBy})"

        if assignTo is not None and snippet is not None:
            snippet = f"{assignTo} = {snippet}"

        return snippet
