"""
Class module for the `Parse` class.
Please see `help(Parse)` for more information.
"""

from __future__ import annotations

from typing import Sequence, Dict, Any, Union
from collections import defaultdict

from .rules import Rule, LexicalRule, AnnotatorRule


class Parse:
    """
    Utility class representing a parse tree of a natural language utterance.

    A parse tree consists of nodes which are represented by rules. Each parse
    stores the corresponding rule of its root nodes as well as the parses
    representing the sub-trees connected to its root node.
    """

    def __init__(self, rule: Rule, children: Sequence[Union[str, Parse]]):
        """Please see `help(Parse)` for more information."""
        self.rule: Rule = rule
        self.children: Sequence[Union[str, Parse]] = tuple(children[:])
        self.semantics: Dict[str, Any] = self.computeSemantics()

    def __str__(self):
        return "(" + self.rule.lhs + " " + " ".join([str(c) for c in self.children]) + ")"

    def computeSemantics(self) -> Dict[str, Any]:
        """Compute the semantics for this parse."""
        semantic = None
        if isinstance(self.rule, LexicalRule):
            # NOTE: if the rule is lexial, the semantic must be Simple,
            # so the function arguments do not matter
            semantic = self.rule.semantic.compute(None)
        elif isinstance(self.rule, AnnotatorRule):
            semantic = self.rule.semantic.compute(self.children)
        else:
            childSemantics = [child.semantics for child in self.children]
            semantic = self.rule.semantic.compute(childSemantics)

        return semantic

    def ruleFeatures(self):
        """
        Create a dictionary from (string representations of) rules to how often they were
        used in the given parse.
        """
        def collectRuleFeatures(parse, features):
            feature = str(parse.rule)
            features[feature] += 1
            for child in parse.children:
                if isinstance(child, Parse):
                    collectRuleFeatures(child, features)

        features = defaultdict(int)
        collectRuleFeatures(self, features)

        return features

    def rulePrecedenceFeatures(self):
        """
        Create a dictionary from rule pairs mapped to the count of their occurence.

        Rule pairs consists of two rules (A, B) where rule A is used directly after rule B
        in the parse tree, meaning that rule A is the corresponding rule to the node directly
        above the node of rule B.
        Rule pairs are represented as a tuple of their corresponding string representation.
        """
        def collectFeatures(parse, features):
            for child in parse.children:
                if isinstance(child, Parse):
                    features[(str(parse.rule), str(child.rule))] += 1
                    collectFeatures(child, features)

        features = defaultdict(float)
        collectFeatures(self, features)

        return features

    def featureVector(self, grammar):
        """A feature vector of rule occurences used for scoring this parse."""
        ruleFeatureDict = grammar.ruleFeatureDict

        parseRuleFeatureDict = self.ruleFeatures()
        for ruleAsString, frequency in parseRuleFeatureDict.items():
            ruleFeatureDict[ruleAsString] += frequency

        parsePrecedenceRuleFeatureDict = self.rulePrecedenceFeatures()
        for ruleTuple, frequency in parsePrecedenceRuleFeatureDict.items():
            ruleFeatureDict[ruleTuple] += frequency

        return [frequency for ruleAsString, frequency in ruleFeatureDict.items()]
